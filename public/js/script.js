function hideElement(element) {
    if (element != null) {
        setTimeout(function() {
            element.style.display = 'none';
        }, 5000);
    }
}

var index_msg = document.body.querySelector('#success-section');
var adminAccept = document.body.querySelector('#accept-reservation');
var adminReject = document.body.querySelector('#reject-reservation');

hideElement(index_msg);
hideElement(adminAccept);
hideElement(adminReject);