# Hotel Reservation System

Hi! This is a guide for deploying my web project. I am going to explain how to deploy this project using any Linux machine (I recommend to use a Debian-based distribution).

## Linux Machine
For this tutorial I will explain the steps using Ubuntu 18.04, but you can use any other Linux distro (preferably a Debian-based distro).

### Step 1
Open a terminal and enter the following command in order to install PHP and its dependencies:

`sudo apt-get install php7.2 php-mbstring php-xml php-sqlite3 php-curl php-xdebug`
    
After that, enter the command `sudo apt-get install git` to install Git.

### Step 2
Clone this repository using the command `git clone https://gitlab.com/dsilveira2902/hotel-reservation-system.git`

### Step 3
Enter the command `cd hotel-reservation-system` and press the **Enter** key.

### Step 4
Create the file **database/database.sqlite** using the command `touch database/database.sqlite`

### Step 5
Enter the command `php artisan migrate:refresh --seed` in order to create the tables and relationships in the database.

### Step 6
Write the command `php artisan serve` and press the **Enter** key:

### Step 7
Open a web browser (I recommend you to use Google Chrome) and write the URL http://127.0.0.1:8000

> **Note:** In order to test correctly the application, follow the next steps:
> 
> Step 1. Select the room you want to reserve.

> Step 2. Click on the **Add to cart** button and you will be taken to your shopping cart.

> Step 3. Once in the shopping cart you can select the number of nights you want to stay in the hotel.

> Step 4. Click on the **Checkout** button and you will be taken to the checkout form.

> Step 5. Fill correctly the checkout form and click on the **Confirm** button.

> Step 6. Click on the login link at the top of the page.

> Step 7. For the login form use the following credentials:

> **User:** admin@example.com

> **Password:** admin

> Step 8. Now, click on the **Admin** dropdown menu at the top of the page and select the option **Reservations List**.

> Step 9. On the **Status** column, click on the accept or reject button and you will receive and email indicating the status of your reservation.