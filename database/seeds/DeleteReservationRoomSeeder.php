<?php

use Illuminate\Database\Seeder;

class DeleteReservationRoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservation_room')->truncate();
    }
}
