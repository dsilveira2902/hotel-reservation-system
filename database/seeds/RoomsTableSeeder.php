<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ruta_imgs = '/img/rooms/';
        
        DB::table('rooms')->insert([
            'name' => 'Gr8 Room',
            'price' => 60.00,
            'picture' => $ruta_imgs . 'gr8-room.jpg',
            'description' => '<p>The comfortable Gr8 Room (24,3m<sup>2</sup>) is designed to be spacious, contemporary and cosy. The room has a double box spring bed, generously-sized bathroom with shower, hair dryer and complimentary toiletries. It also has a separate toilet, a table with two armchairs, wardrobe space and a flat screen TV. WiFi is available without charge around the clock. Breakfast is included and is provided by La Place.</p><h5><strong>Gr8 Room Facilities</strong></h5><ul><li>Boxspring bed</li><li>Bathroom with shower</li><li>Separate toilet</li><li>Free WiFi</li><li>Flatscreen TV</li><li>Free amenities</li><li>Hair dryer</li><li>Table with two armchairs</li><li>Safe</li>',
        ]);

        DB::table('rooms')->insert([
            'name' => 'Gr8 Family Room',
            'price' => 90.00,
            'picture' => $ruta_imgs . 'gr8-family-room.jpg',
            'description' => '<p>The Gr8 Family Room is the perfect choice for families with children. You get two interconnecting Gr8 Rooms, giving you access to no less than 31,4 m<sup>2</sup>. The comfortable Family Room is designed to be spacious and is contemporary and cosy. The Gr8 Family Room has a double box spring bed, one generously-sized bathroom with shower, hair dryer and complimentary toiletries. They also have a separate toilet, a table with two armchairs, wardrobe space and a flat screen TV. High-speed WiFi is available without charge around the clock. Breakfast is included and is provided by La Place.</p><h5><strong>Gr8 Family Room Facilities</strong></h5><ul><li>Interconnecting rooms</li><li>Bathroom with shower</li><li>Boxspring bed</li><li>Separate toilet</li><li>Free WiFi</li><li>Flatscreen TV</li><li>Free amenities</li><li>Hair dryer</li><li>Table with two armchairs</li><li>Safe</li></ul>',
        ]);

        DB::table('rooms')->insert([
            'name' => 'Gr8 Suite',
            'price' => 120.00,
            'picture' => $ruta_imgs . 'gr8-suite.jpg',
            'description' => '<p>If you would like to have even more space, then the Gr8 Suite (42 m<sup>2</sup>) is the ideal choice. The comfortable suites have corner locations, so they feel very spacious. The Gr8 Suites are contemporary and cosy, with a double box spring bed, generously-sized bathroom with shower, hair dryer and complimentary toiletries. They also have a separate toilet, a table with two armchairs, wardrobe space and a flat screen TV. High-speed WiFi is available without charge around the clock. Breakfast is included and is provided by La Place.</p><h5><strong>Gr8 Suite Facilities</strong></h5><ul><li>Located on the corners</li><li>Bathroom with shower</li><li>Boxspring bed</li><li>Separate toilet</li><li>Free WiFi</li><li>Flatscreen TV</li><li>Free amenities</li><li>Hair dryer</li><li>Table with two armchairs</li><li>Safe</li></ul>',
        ]);
    }
}
