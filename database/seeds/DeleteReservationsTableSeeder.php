<?php

use Illuminate\Database\Seeder;

class DeleteReservationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservations')->truncate();
    }
}
