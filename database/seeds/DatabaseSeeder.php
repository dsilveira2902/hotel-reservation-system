<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Call other seed files:
        $this->call(DeleteUsersSeeder::class);
        $this->call(DeleteReservationRoomSeeder::class);
        $this->call(DeleteRoomsTableSeeder::class);
        $this->call(DeleteReservationsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RoomsTableSeeder::class);

        // Show information in the console:
        $this->command->info('Users table cleaned!');
        $this->command->info('Reservation-Room table cleaned!');
        $this->command->info('Rooms table cleaned!');
        $this->command->info('Reservations table cleaned!');
        $this->command->info('Users table seeded!');
        $this->command->info('Rooms table seeded!');
    }
}
