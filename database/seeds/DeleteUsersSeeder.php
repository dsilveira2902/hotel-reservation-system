<?php

use Illuminate\Database\Seeder;

class DeleteUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
    }
}
