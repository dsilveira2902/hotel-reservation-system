<?php

use Illuminate\Database\Seeder;

class DeleteRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->truncate();
    }
}
