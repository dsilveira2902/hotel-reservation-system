<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Shows the landing page.
Route::get('/', function () {
    return view('index');
});

// Shows all the rooms.
Route::get('/rooms', 'RoomsController@index');

// Shows a determined room.
Route::get('/rooms/{room_id}', 'RoomsController@show');

Route::get('/rooms/{room_id}/reservation', 'RoomsController@book_room');

Route::get('/admin', 'ReservationsController@index')->middleware('adminFilter');

Auth::routes();

// Shopping cart
Route::get('/rooms/{room_id}/reservation/add_to_cart', 'ShoppingController@addToCart');

Route::get('/shopping_cart', 'ShoppingController@showCart');

Route::get('/reduceQty/{id}', 'ShoppingController@reduceQuantity');

Route::get('/incrementQty/{id}', 'ShoppingController@incrementQuantity');

Route::get('/reduceDays/{id}', 'ShoppingController@reduceDays');

Route::get('/incrementDays/{id}', 'ShoppingController@incrementDays');

Route::get('/remove/{id}', 'ShoppingController@removeItem');

Route::get('/shopping_cart/empty', 'ShoppingController@emptyCart');

// Checkout
Route::get('/checkout', 'ReservationsController@getCheckout');

Route::post('/checkout', 'ReservationsController@postCheckout');

Route::get('/admin/{reservation_id}/accept', 'ReservationsController@acceptReservation');

Route::get('/admin/{reservation_id}/reject', 'ReservationsController@rejectReservation');