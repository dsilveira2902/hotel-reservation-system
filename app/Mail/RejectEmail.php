<?php

namespace App\Mail;

use App\Reservation;
use App\Cart;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RejectEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $reservation;
    protected $cart;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Reservation $reservation, Cart $cart)
    {
        $this->reservation = $reservation;
        $this->cart = $cart;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('admin@example.com', 'MyHotel')
                    ->subject("Your Reservation")
                    ->markdown('admin.rejectEmail')
                    ->with([
                        'reservation' => $this->reservation,
                        'people' => $this->reservation->num_people,
                        'items' => $this->cart->items,
                    ]);
    }
}
