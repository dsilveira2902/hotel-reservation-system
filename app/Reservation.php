<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Reservation extends Model
{
    public $timestamps = false;

    public function rooms() {
        return $this->belongsToMany('App\Room');
    }
}