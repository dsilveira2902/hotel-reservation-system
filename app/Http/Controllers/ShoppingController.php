<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cart;
use App\Room;

class ShoppingController extends Controller
{
    public function addToCart($room_id, Request $request) {
        $room = Room::findOrFail($room_id);
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($room, $room->id);

        request()->session()->put('cart', $cart);

        return redirect('/shopping_cart');
    }

    public function incrementQuantity($id) {
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->incrementByOne($id);

        if (count($cart->items) > 0) {
            session()->put('cart', $cart);
        }
        else {
            session()->forget('cart');
        }

        return redirect('/shopping_cart');
    }

    public function reduceQuantity($id) {
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);

        if (count($cart->items) > 0) {
            session()->put('cart', $cart);
        }
        else {
            session()->forget('cart');
        }

        return redirect('/shopping_cart');
    }

    public function removeItem($id) {
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);

        if (count($cart->items) > 0) {
            session()->put('cart', $cart);
        }
        else {
            session()->forget('cart');
        }

        return redirect('/shopping_cart');
    }

    public function showCart() {
        if (!session()->has('cart')) {
            return view('shoppingCart');
        }
        else {
            $oldCart = session()->get('cart');
            $cart = new Cart($oldCart);

            return view('shoppingCart', ['rooms' => $cart->items, 'totalPrice' => $cart->totalPrice]);
        }
    }

    public function emptyCart() {
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->emptyCart();

        return view('shoppingCart', ['rooms' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }
}
