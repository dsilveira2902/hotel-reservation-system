<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Reservation;
use App\Room;
use App\Cart;
use App\Http\Requests\ReservationRequest;
use App\Http\Controllers\Controller;
use App\Mail\AcceptEmail;
use App\Mail\RejectEmail;
use Illuminate\Support\Facades\Mail;

class ReservationsController extends Controller
{
    public function index() {
        $reservations = Reservation::all();
        return view('admin.reservations', ['reservations' => $reservations]);
    }

    public function getCheckout() {
        if (!session()->has('cart')) {
            return view('shoppingCart');
        }

        $oldCart = session()->get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;

        return view('rooms.reservation', ['total' => $total, 'cart' => $cart]);
    }

    public function postCheckout(ReservationRequest $request) {
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);

        try {
            $reservation = new Reservation();
            $reservation->name = request()->name;
            $reservation->email = request()->email;
            $reservation->birthday = request()->birthday;
            $reservation->phone = request()->phone;
            $reservation->total_price = $cart->totalPrice;
            $reservation->num_people = request()->people;
            $reservation->date = request()->date;
            $reservation->comments = request()->comments;

            $reservation->save();

            foreach($cart->items as $item) {
                $reservation->rooms()->attach($item['item']['id']);
            }

            return redirect('/')->with('success_message', 'Your reservation is being processed. You will receive an email from the admin in a few minutes.');
        
        } catch(Error $e) {
            return back()->withErrors(['Error! ' . $e->getMessage()]);
        }
    }

    public function acceptReservation($id) {
        $res = Reservation::findOrFail($id);
        $res->status = true;
        $res->save();

        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);

        // Send email with accept.
        Mail::to($res->email)->send(new AcceptEmail($res, $cart));

        session()->forget('cart');

        return redirect('/admin')->with('accept_reservation', 'Reservation accepted. An email will be sent to the client.');
    }

    public function rejectReservation($id) {
        $res = Reservation::findOrFail($id);
        $res->status = false;
        $res->save();

        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);

        // Send email with reject.
        Mail::to($res->email)->send(new RejectEmail($res, $cart));

        session()->forget('cart');

        return redirect('/admin')->with('reject_reservation', 'Reservation rejected. An email will be sent to the client.');
    }
}