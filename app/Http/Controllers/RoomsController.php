<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Room;

class RoomsController extends Controller
{
    public function index() {
        $rooms = Room::all();

        return view('rooms.rooms', ['rooms' => $rooms]);
    }

    public function show($id) {
        $room = Room::findOrFail($id);

        return view('rooms.room', ['room' => $room]);
    }

    public function book_room($id) {
        $room = Room::findOrFail($id);

        return view('rooms.reservation', ['room' => $room]);
    }
}