<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|string|email|max:255|',
            'birthday' => 'required|date|before_or_equal:today',
            'people' => 'required|numeric|min:1',
            'date' => 'required|date|after_or_equal:today',
            'payment' => 'required',
            'card_name' => 'required|string',
            'card_number' => 'required|digits:16',
            'expiration_month' => 'required',
            'expiration_year' => 'required',
            'CVV' => 'required|numeric|digits:3'
        ];
    }

    public function messages() {
        return [
            'people.required' => 'The number people field is required.',
            'people.numeric' => 'The number people field must be numeric.',
            'people.min' => 'The minimum value for number people field must be 1.',
            'date.required' => 'The reservation date field is required.',
            'date.date' => 'The reservation date field must be a date.',
            'date.after_or_equal' => 'The reservation date cannot be a past date.',
            'CVV.required' => 'The CVV field is required.',
            'CVV.numeric' => 'The CVV field must be a number.',
            'CVV.digits' => 'The CVV field must have 3 digits.',
        ];
    }
}
