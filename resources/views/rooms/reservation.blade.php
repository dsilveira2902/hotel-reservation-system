@extends('layouts.layout')

@section('title')
    MyHotel - Reservation
@endsection

@section('content')

    <div class="container">
        <h1 id="room-title">Reservation</h1>

        @if(count($errors) > 0)
            <div id="errors-section" class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li class="alert-panel">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div id="reservation-form" class="container">
            <div class="row text-center">
                <div id="room-description" class="col-lg-6 col-md-6 mb-4">
                    <form action="/checkout" method="POST">
                        {{ csrf_field() }}

                        <hr>

                        <h4 class="mb-3">Personal information</h4>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Name:</label><span class="required"> *</span>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="email">Email:</label><span class="required"> *</span>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="birthday">Birthday:</label><span class="required"> *</span>
                                <input type="date" class="form-control" id="birthday" name="birthday" value="{{ old('birthday') }}">
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="phone">Phone number:</label>
                                <input type="tel" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                            </div>
                        </div>

                        <hr>

                        <h4 class="mb-3">Reservation information</h4>

                        <div class="form-row">
                            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="num-people">Number people:</label><span class="required"> *</span>
                                <input type="number" class="form-control" id="num-people" name="people" value="{{ old('people') }}">
                            </div>

                            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="res-date">Reservation date:</label><span class="required"> *</span>
                                <input type="date" class="form-control" id="res-date" name="date" value="{{ old('date') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="comments">Comments:</label>
                            <textarea class="form-control" id="comments" name="comments" rows="3">{{ old('comments') }}</textarea>
                        </div>

                        <hr>

                        <h4 class="mb-3">Payment<span class="required"> *</span></h4>
                    
                        <div class="form-check{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input class="form-check-input" type="radio" id="radio1" name="payment">
                            <label class="form-check-label" for="radio1">Credit card</label>
                        </div>

                        <div class="form-check{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input class="form-check-input" type="radio" id="radio2" name="payment">
                            <label class="form-check-label" for="radio2">Debit card</label>
                        </div>

                        <div class="form-check{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input class="form-check-input" type="radio" id="radio3" name="payment">
                            <label class="form-check-label" for="radio3">PayPal</label>
                        </div>
                        <br>

                        <div class="form-row">
                            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="card-name">Name on card:</label><span class="required"> *</span>
                                <input type="text" class="form-control" id="card-name" name="card_name" value="{{ old('card_name') }}">
                                <small class="text-muted">Full name as displayed on card</small>
                            </div>
        
                            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="card-number">Credit card number:</label><span class="required"> *</span>
                                <input type="text" class="form-control" id="card-number" name="card_number" value="{{ old('card_number') }}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-4 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="expiration">Expiration date:</label><span class="required"> *</span>
                            
                                <select name="expiration_month" style="height:35px;">
                                    <option value="">MM</option>
                                    <option value="1" {{ old('expiration_month') == "1" ? 'selected' : '' }}>01</option>
                                    <option value="2" {{ old('expiration_month') == "2" ? 'selected' : '' }}>02</option>
                                    <option value="3" {{ old('expiration_month') == "3" ? 'selected' : '' }}>03</option>
                                    <option value="4" {{ old('expiration_month') == "4" ? 'selected' : '' }}>04</option>
                                    <option value="5" {{ old('expiration_month') == "5" ? 'selected' : '' }}>05</option>
                                    <option value="6" {{ old('expiration_month') == "6" ? 'selected' : '' }}>06</option>
                                    <option value="7" {{ old('expiration_month') == "7" ? 'selected' : '' }}>07</option>
                                    <option value="8" {{ old('expiration_month') == "8" ? 'selected' : '' }}>08</option>
                                    <option value="9" {{ old('expiration_month') == "9" ? 'selected' : '' }}>09</option>
                                    <option value="10" {{ old('expiration_month') == "10" ? 'selected' : '' }}>10</option>
                                    <option value="11" {{ old('expiration_month') == "11" ? 'selected' : '' }}>11</option>
                                    <option value="12" {{ old('expiration_month') == "12" ? 'selected' : '' }}>12</option>
                                </select>
                                /
                                <select name="expiration_year" style="height:35px;">
                                    <option value="">YYYY</option>
                                    <option value="2019" {{ old('expiration_year') == "2019" ? 'selected' : '' }}>2019</option>
                                    <option value="2020" {{ old('expiration_year') == "2020" ? 'selected' : '' }}>2020</option>
                                    <option value="2021" {{ old('expiration_year') == "2021" ? 'selected' : '' }}>2021</option>
                                    <option value="2022" {{ old('expiration_year') == "2022" ? 'selected' : '' }}>2022</option>
                                    <option value="2023" {{ old('expiration_year') == "2023" ? 'selected' : '' }}>2023</option>
                                    <option value="2024" {{ old('expiration_year') == "2024" ? 'selected' : '' }}>2024</option>
                                </select>
                            </div>

                            <div class="col-md-2 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="CVV">CVV:</label><span class="required"> *</span>
                                <input type="text" class="form-control" id="CVV" name="CVV">
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-success btn-lg btn-block">Confirm</button>
                        </div>
                    </form>
                </div>

                <div class="col-lg-4 col-md-6 mb-4">
                    <div id="fixed-pos" class="card">
                        <div class="card-body">
                            <h4 class="card-title">Total Price:</h4>
                            <p id="bigger" class="card-text"><strong>{{ money_format("%.2n", $total) }} €</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
  <footer class="py-3 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; MyHotel 2019</p>
    </div>
  </footer>
@endsection