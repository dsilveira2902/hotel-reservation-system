@extends('layouts.layout')

@section('title')
    MyHotel - Rooms
@endsection

@section('content')
  <div class="container">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    	<ol class="carousel-indicators">
    		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  		</ol>
  		
  		<div class="carousel-inner">
    		<div class="carousel-item active">
      			<img class="d-block w-100" src="{{ asset('/img/rooms/carousel/room1.jpg') }}" style="max-height: 500px;" alt="First slide">
    		</div>
    		
    		<div class="carousel-item">
      			<img class="d-block w-100" src="{{ asset('/img/rooms/carousel/room2.jpg') }}" style="max-height: 500px;" alt="Second slide">
    		</div>
    		
    		<div class="carousel-item">
      			<img class="d-block w-100" src="{{ asset('/img/rooms/carousel/room3.jpg') }}" style="max-height: 500px;" alt="Third slide">
    		</div>
  		</div>
  
  		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    		<span class="sr-only">Previous</span>
  		</a>
  	
  		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    		<span class="carousel-control-next-icon" aria-hidden="true"></span>
    		<span class="sr-only">Next</span>
  		</a>
	  </div>

    <div class="row text-center">
      <div class="col-lg-12 col-md-6 mb-4">
        <h1 id="rooms-title">Rooms</h1>
      </div>
    </div>

    <div class="row text-center">
      @foreach($rooms as $room)
        <div class="col-lg-4 col-md-6 mb-4">
          <div class="card h-100">
            <a href="{{ url('/rooms/' . $room->id) }}"><img src="{{ url($room->picture) }}" class="card-img-top" alt="{{ $room->name }}" title="{{ $room->name }}"></a>
            <div class="card-body">
              <a href="{{ url('/rooms/' . $room->id) }}" style="text-decoration: none;"><h4 class="card-title text-muted">{{ $room->name }}</h4></a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection

@section('footer')
  <footer class="py-3 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; MyHotel 2019</p>
    </div>
  </footer>
@endsection