@extends('layouts.layout')

@section('title')
    MyHotel - {{ $room->name }}
@endsection

@section('content')

    <div class="container">
        <img id="room-img" src="{{ url($room->picture) }}" width="100%">

        <div class="row text-center">
            <div id="room-description" class="col-lg-9 col-md-6 mb-4">
                <h1 class="card-title">{{ $room->name }}</h1>
                <p class="card-text">{!! $room->description !!}</p>
            </div>

            <div id="room-reservation" class="col-lg-3 col-md-6 mb-4">
                <h4 class="card-title">Price:</h4>
                <p class="card-text">{{ number_format($room->price, 2, '.', '') }} € per night</p>
                <a class="btn btn-success" href="/rooms/{{ $room->id }}/reservation/add_to_cart">Add to cart</a>
            </div>
        </div>
    </div>
@endsection

@section('footer')
  <footer class="py-3 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; MyHotel 2019</p>
    </div>
  </footer>
@endsection