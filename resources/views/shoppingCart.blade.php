@extends('layouts.layout')

@section('title')
    Shopping cart
@endsection

@section('content')

    @if(session()->has('cart'))

        @if (session()->get('cart')->totalQuantity == 0)
            <div class="text-center">
                <div class="col-lg-12 col-md-6 mb-4">
                    <h1 id="rooms-title">Your shopping cart is empty! :(</h1>
                </div>
            </div>

            @section('footer')
                <footer id="footer-login" class="py-3 bg-dark">
                    <div class="container">
                        <p class="m-0 text-center text-white">Copyright &copy; MyHotel 2019</p>
                    </div>
                </footer>
            @endsection
        @else
            <div id="shopping-cart" class="table-responsive-lg">
                <h2>Your Cart:</h2>

                <table class="table table-bordered">
                    <thead>
                        <tr class="table-primary">
                            <th style="text-align: center; width: 15%;" scope="col">Picture</th>
                            <th style="text-align: center; width: 19%;" scope="col">Name</th>
                            <th style="text-align: center; width: 13%;" scope="col">Number of nights</th>
                            <th style="text-align: center; width: 12%;" scope="col">Price per night</th>
                            <th style="text-align: center; width: 15%;" scope="col">Total Price</th>
                            <th style="text-align: center; width: 10%;" scope="col">Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($rooms as $room)
                            <tr>
                                <td class="text-centered" style="width: 15%;"><img id="cart-img" alt="{{ $room['item']['name'] }}" title="{{ $room['item']['name'] }}" src="{{ $room['item']['picture'] }}" height="18%" alt="{{ $room['item']['name'] }}"></td>
                                <td class="text-centered" style="vertical-align: middle; width: 19%;">{{ $room['item']['name'] }}</td>
                                <td class="text-centered" style="vertical-align: middle; width: 13%;">
                                    <a class="shopping_icon" href="/reduceQty/{{ $room['item']['id'] }}" style="margin-right:10px;" title="Reduce 1 unit">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </a>

                                    {{ $room['quantity'] }}

                                    <a class="shopping_icon" href="/incrementQty/{{ $room['item']['id'] }}" style="margin-left:10px;" title="Increment 1 unit">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </td>
                                
                                <td class="text-centered" style="vertical-align: middle; width: 12%;">{{ money_format("%.2n", $room['item']['price']) }} €</td>           
                                <td class="text-centered" style="vertical-align: middle; width: 15%;">{{ money_format("%.2n", $room['price']) }} €</td>

                                <td class="text-centered" style="vertical-align:middle; width: 10%;">
                                    <a class="shopping_icon" href="/remove/{{ $room['item']['id'] }}" title="Remove all">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        
                        <tr class="text-centered table-info" style="font-size:20px;">
                            <td class="shopping_cart total_price" colspan="4"><strong>Total price:</strong></td>
                            <td class="shopping_cart total_price"><strong>{{ money_format("%.2n", $totalPrice) }} €</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="shopping-buttons" class="text-center">
                <a class="btn btn-danger" href="/shopping_cart/empty">Empty cart</a>
                <a class="btn btn-success" href="/checkout">Checkout</a>
            </div>

            @section('footer')
                <footer class="py-3 bg-dark">
                    <div class="container">
                        <p class="m-0 text-center text-white">Copyright &copy; MyHotel 2019</p>
                    </div>
                </footer>
            @endsection
        @endif
    @else
        <div class="text-center">
            <div class="col-lg-12 col-md-6 mb-4">
                <h1 id="rooms-title">Your shopping cart is empty! :(</h1>
            </div>
        </div>

        @section('footer')
            <footer id="footer-login" class="py-3 bg-dark">
                <div class="container">
                    <p class="m-0 text-center text-white">Copyright &copy; MyHotel 2019</p>
                </div>
            </footer>
        @endsection
    @endif
@endsection