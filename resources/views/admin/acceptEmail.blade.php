@component('mail::message')
# Reservation Confirmation

Hello {{ $reservation->name }}.<br>

This is a confirmation email for your reservation at MyHotel.<br>

Below you can see the details of your reservation:

<ul>
    <li>Reservation date: {{ date("d/m/Y", strtotime($reservation->date)) }}</li>
    <li>Price (EUR): {{ money_format("%.2n", $reservation->total_price) }} €</li>
    <li>Number of people: {{ $reservation->num_people }}</li>
</ul>

@component('mail::table')
        |             Room            |     Number of nights    |                    Price per night                   |
        |:---------------------------:|:-----------------------:|:----------------------------------------------------:|
    @foreach ($items as $item)
        | {{ $item['item']['name'] }} | {{ $item['quantity'] }} | {{ money_format("%.2n", $item['item']['price']) }} € |
    @endforeach
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent