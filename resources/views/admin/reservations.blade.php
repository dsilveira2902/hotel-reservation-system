@extends ('layouts.layout')

@section('title')
    MyHotel - Manage Reservations
@endsection

@section('content')
    @if(DB::table('reservations')->count() == 0)
        <div class="text-center">
            <div class="col-lg-12 col-md-6 mb-4">
                <h1 id="rooms-title">The reservations list is empty! :(</h1>
            </div>
        </div>

        @section('footer')
            <footer id="footer-login" class="py-3 bg-dark">
                <div class="container">
                    <p class="m-0 text-center text-white">Copyright &copy; MyHotel 2019</p>
                </div>
            </footer>
        @endsection
    @else

        @if(session()->has('accept_reservation'))
            <div id="accept-reservation" class="alert" role="alert">
                {{ session()->get('accept_reservation') }}
            </div>
        @elseif(session()->has('reject_reservation'))
            <div id="reject-reservation" class="alert" role="alert">
                {{ session()->get('reject_reservation') }}
            </div>
        @endif

        <div id="reservations-list" class="table-responsive-lg">
            <h2>Reservations List</h2>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 7%" class="text-centered" scope="col">Total (EUR)</th>
                        <th style="width: 8%" class="text-centered" scope="col">Reservation date</th>
                        <th style="width: 4%" class="text-centered" scope="col">Num. People</th>
                        <th style="width: 10%" class="text-centered" scope="col">Name</th>
                        <th style="width: 15%" class="text-centered" scope="col">Email</th>
                        <th style="width: 8%" class="text-centered" scope="col">Phone</th>
                        <th style="width: 10%" class="text-centered" scope="col">Birthday</th>
                        <th style="width: 20%" class="text-centered" scope="col">Comments</th>
                        <th style="width: 10%" class="text-centered" scope="col">Status</th>
                    </tr>
                </thead>
            
                <tbody>
                    @foreach($reservations as $res)
                        <tr>
                            <td style="width: 7%" class="text-centered">{{ money_format("%.2n", $res->total_price) }} €</td>
                            <td style="width: 8%" class="text-centered">{{ date("d/m/Y", strtotime($res->date)) }}</td>
                            <td style="width: 4%" class="text-centered">{{ $res->num_people }}</td>
                            <td style="width: 10%" class="text-centered">{{ $res->name }}</td>
                            <td style="width: 15%" class="text-centered">{{ $res->email }}</td>
                            <td style="width: 8%" class="text-centered">{{ $res->phone }}</td>
                            <td style="width: 10%" class="text-centered">{{ date("d/m/Y", strtotime($res->birthday)) }}</td>
                            <td style="width: 20%" class="text-centered">{{ $res->comments }}</td>
                            <td style="width: 10%" class="text-centered">
                                @if (is_null($res->status))
                                    <a class="btn btn-success" href="/admin/{{ $res->id }}/accept" role="button" title="Accept"><i class="fa fa-check"></i></a>
                                    <a class="btn btn-danger" href="/admin/{{ $res->id }}/reject" role="button" title="Reject"><i class="fa fa-close"></i></a>
                                @elseif($res->status)
                                    <span id="accepted">ACCEPTED</span>
                                @else
                                    <span id="rejected">REJECTED</span>
                                @endif
                            </td>
                            
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection

@section('footer')
  <footer class="py-3 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; MyHotel 2019</p>
    </div>
  </footer>
@endsection