@extends('layouts.layout')

@section('title')
  Welcome to MyHotel!
@endsection

@section('content')

  @if(session()->has('success_message'))
    <div id="success-section" class="alert" role="alert">
      {{ session()->get('success_message') }}
    </div>
  @endif

  <div class="container">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    	<ol class="carousel-indicators">
    		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  		</ol>
  		
  		<div class="carousel-inner">
    		<div class="carousel-item active">
      			<img class="d-block w-100" src="{{ asset('/img/hotel/1.jpg') }}" style="height: 500px;" alt="First slide">
    		</div>
    		
    		<div class="carousel-item">
      			<img class="d-block w-100" src="{{ asset('/img/hotel/2.jpg') }}" style="height: 500px;" alt="Second slide">
    		</div>
    		
    		<div class="carousel-item">
      			<img class="d-block w-100" src="{{ asset('/img/hotel/3.jpg') }}" style="height: 500px;" alt="Third slide">
    		</div>
  		</div>
  
  		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    		<span class="sr-only">Previous</span>
  		</a>
  	
  		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    		<span class="carousel-control-next-icon" aria-hidden="true"></span>
    		<span class="sr-only">Next</span>
  		</a>
	  </div>

    <div class="row text-center">
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <div class="card-body">
            <h4 class="card-title">Hotel</h4>
            <p class="card-text">Come alone or bring your family with you, stay here for a night or for weeks, stay here while on business trip or at some kind of conference - either way our hotel is the best possible variant. Feel free to contact us anytime in case you have any questions or concerns. We're always glad to see you in our hotel.</p>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <div class="card-body">
            <h4 class="card-title">Hotel Overview</h4>
            <p class="card-text">3 stars</p>
            <p class="card-text">Goudseweg 32<br>2411 HL, Bodegraven<br>The Netherlands</p>
            <p class="card-text">Email: reservations@gr8hotels.nl</p>
            <p class="card-text">Phone: +31 (0)88 45 40 631</p>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 mb-4">
        <div class="card h-100">
          <div class="card-body">
            <h4 class="card-title">Location</h4>

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2772.260949689115!2d4.7387413!3d52.067096199999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c5d7164e595555%3A0x42505a4c2d3cf391!2sGr8%20Hotel%20Bodegraven!5e1!3m2!1ses!2ses!4v1571331146485!5m2!1ses!2ses" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer')
  <footer class="py-3 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; MyHotel 2019</p>
    </div>
  </footer>
@endsection