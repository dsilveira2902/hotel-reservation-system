<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="This is an example of a page of a hotel reservation system">
        <meta name="author" content="Diego Silveira">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <link href="{{ asset('/css/style.css') }}" rel="stylesheet" media="all">
        <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" media="all">
        <link rel="stylesheet" href="{{ url('https://use.fontawesome.com/releases/v5.7.0/css/all.css') }}" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') }}">
    </head>

    <body>
        {{-- start navbar --}}
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">MyHotel</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">        
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/rooms') }}">Rooms</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/shopping_cart') }}">My Cart</a>
                        </li>

                        @guest
                            <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                        @else
                            <li class="nav-item dropdown show">
                                <a id="username" class="nav-link dropdown-toggle" href="#" role="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }}
                                </a>
                                
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    @if (\App\User::isAdmin(Auth::user()->id))
                                        <a class="dropdown-item" href="{{ url('/admin') }}">Reservations List</a>
                                    @endif

                                    <a id="logout-link" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        {{-- end navbar --}}

        {{-- start content --}}
        @yield('content')
        {{-- end content --}}

        {{-- start footer --}}
        @yield('footer')
        {{--end footer --}}

        <script src="{{ asset('/js/script.js') }}"></script>
        <script src="{{ asset('/js/jquery.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
    </body>
</html>